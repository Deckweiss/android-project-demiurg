package de.blogspot.deckweiss.demiurg.world;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.List;

import de.blogspot.deckweiss.demiurg.R;

public class NodeDisplay extends SurfaceView implements Runnable {

    /**
     * Distance from one corner to the opposite corner of a single hexagon
     */
    private static final float hexagonSize = 100;
    private static final float hexagonWidth = ((float) Math.tan(60 * Math.PI / 180) * hexagonSize / 2); //we should use better names to distinguish these two

    Node node;
    HexagonMap map;
    Context context;
    private Bitmap test;
    private Thread t = null;
    private SurfaceHolder holder;
    private Canvas c;
    private Paint wowPaint, borderPaint, textPaint, blackPaint;
    private boolean isItOK;
    private float xDpiScale, yDpiScale;
    private int dpiScale;
    private int hexagonMapSizeX, hexagonManSizeY;
    private float xShift = 0;
    private float yShift = 0;

    public NodeDisplay(Context applicationContext, AttributeSet attrs) {
        super(applicationContext, attrs);
        context = applicationContext;
        holder = getHolder();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        xDpiScale = metrics.xdpi / 240;
        yDpiScale = metrics.ydpi / 240;
        dpiScale = metrics.densityDpi;
        //Log.d("dpi", "dpi:"+metrics.densityDpi+" x:"+metrics.xdpi+" y:"+metrics.ydpi); //Test shows: dpi:240 x:240.0 y:240.0

        test = prepareBmp(R.drawable.test);
        this.prepareTestPaint();
    }

    public NodeDisplay(Context applicationContext, AttributeSet attrs, int defStyle) {
        super(applicationContext, attrs, defStyle);
        context = applicationContext;
        holder = getHolder();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        xDpiScale = metrics.xdpi / 240;
        yDpiScale = metrics.ydpi / 240;
        dpiScale = metrics.densityDpi;
        //Log.d("dpi", "dpi:"+metrics.densityDpi+" x:"+metrics.xdpi+" y:"+metrics.ydpi); //Test shows: dpi:240 x:240.0 y:240.0

        test = prepareBmp(R.drawable.test);
        this.prepareTestPaint();
    }

    //<editor-fold desc="Runnable">


    @Override
    public void run() {
        //invalidate
        while (isItOK) {
            if (!holder.getSurface().isValid()) {
                continue;
            }

            try {
                Thread.sleep(32);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            xShift = NodeActivity.getxShift();
            yShift = NodeActivity.getyShift();

            c = holder.lockCanvas();

            c.drawRect(0 + xShift, 0 + yShift, hexagonMapSizeX + xShift, hexagonManSizeY + yShift, blackPaint);
            drawMap();

            holder.unlockCanvasAndPost(c);
        }
    }

    public void pause() {
        isItOK = false;
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t = null;
    }

    public void resume() {
        isItOK = true;
        t = new Thread(this);
        t.start();
    }


    //</editor-fold>

    /**
     * Draws a hexagon at the specified position, considering the hexagon's flatness and its size in pixels.
     *
     * @param x The x-position to draw at in pixels.
     * @param y The y-position to draw at in pixels.
     */
    public void drawHexagon(int x, int y) {
        Path p = new Path();
        float px, py;

        for (int i = 0; i < 6; i++) {
            px = (float) (x + (hexagonSize / 2) * Math.cos((i + (map.isFlat() ? 0 : 0.5)) * 2 * (Math.PI) / 6));
            py = (float) (y + (hexagonSize / 2) * Math.sin((i + (map.isFlat() ? 0 : 0.5)) * 2 * (Math.PI) / 6));

            if (i == 0) {
                p.moveTo(px, py);
            } else {
                p.lineTo(px, py);
            }
        }
        p.close();
        c.drawPath(p, wowPaint);
        c.drawPath(p, borderPaint);
    }

    /**
     * @see #getHexagonPosition(int, int)
     */
    public void drawHexagon(int[] position) {
        drawHexagon(position[0], position[1]);
    }

    /**
     * Returns the pixel coordinates of the midpoint of a given hexagon of the hexmap, considering the hexagon's flatness and its size in pixels.
     *
     * @param x The x-coordinate of the hexagon on the map.
     * @param y The y-coordinate of the hexagon on the map.
     * @return The hexagon's midpoint in pixels.
     */
    public int[] getHexagonPosition(int x, int y) { //I'd still prefer using a "Position" class in such situations rather than returning a meaningless array of values
        float rx = 0;
        float ry = 0;
        if (map.isFlat()) {
            rx += x * (float) (hexagonWidth * Math.cos(Math.PI / 6));
            ry += x * (float) (hexagonWidth * Math.sin(Math.PI / 6));
            ry += y * hexagonWidth;
        } else {
            rx += y * (float) (hexagonWidth * Math.cos(Math.PI / 3));
            ry += y * (float) (hexagonWidth * Math.sin(Math.PI / 3));
            rx += x * hexagonWidth;
        }
        return new int[]{(int) rx, (int) ry};
    }

    /**
     * Uses the x- and y-coordinates of the given {@code hexagon}.
     *
     * @see #getHexagonPosition(int, int)
     */
    public int[] getHexagonPosition(Hexagon hexagon) {
        return getHexagonPosition(hexagon.getX(), hexagon.getY());
    }


    /**
     * Returns the map's total width and height in pixels.
     *
     * @return The map's total width and height in pixels.
     */
    public int[] getMapSize() { //I'd still prefer using a "Size/Dimension" class in such situations rather than returning a meaningless array of values
        //untested, should work
        int[] dimension = map.getDimension();
        int[] xDimension = getHexagonPosition(dimension[0], 0);
        int[] yDimension = getHexagonPosition(0, dimension[1]);
        return new int[]{
                Math.max(xDimension[0], yDimension[0]),
                Math.max(xDimension[1], yDimension[1])
        };

        /* This will be returned later when we need more than just the absolute width and height of the map
        //untested, should work hopefully
        int[] minXDimension = getHexagonPosition(dimension[0], 0);
        int[] maxXDimension = getHexagonPosition(dimension[1], 0);
        int[] minYDimension = getHexagonPosition(0, dimension[2]);
        int[] maxYDimension = getHexagonPosition(0, dimension[3]);
        return new int[]{
                Math.min(minXDimension[0], minYDimension[0]), //minX
                Math.max(maxXDimension[0], maxYDimension[0]), //maxX
                Math.min(minXDimension[1], minYDimension[1]), //minY
                Math.max(maxXDimension[1], maxYDimension[1]), //maxY
        };
        */
    }

    /**
     * Draws the hexagon map of the node onto the canvas.
     */
    public void drawMap(float offsetX, float offsetY) {
        if (map == null) return;

        c.save();
        //TODO: include translate again
        //using translate will fuck up a background img.

        List<Hexagon> hexagons = map.getHexagons();

        int[] pos;
        float radius = hexagonSize / 2;
        textPaint.setTextSize(radius);
        float textOffsetY = ((textPaint.descent() + textPaint.ascent()) / 2);
        for (int i = 0; i < hexagons.size(); i++) {
            Hexagon hexagon = hexagons.get(i);
            pos = getHexagonPosition(hexagon);
            pos[0] += offsetX + xShift;
            pos[1] += offsetY + yShift;
            drawHexagon(pos);
            if (hexagon.getContent() == HexagonContent.EMPTY) {
                c.drawText("-", pos[0], pos[1] - textOffsetY, textPaint);
            } else if (hexagon.getContent() == HexagonContent.WOOD) {
                c.drawText("W", pos[0], pos[1] - textOffsetY, textPaint);
            } else if (hexagon.getContent() == HexagonContent.WOODCUTTERS_HUT) {
                c.drawText("WC", pos[0], pos[1] - textOffsetY, textPaint);
            }
        }

        c.restore();
    }

    /**
     * {@code offsetX} and {@code offsetY} default to 0 respectively
     *
     * @see #drawMap(float, float)
     */
    public void drawMap() {
        drawMap(c.getWidth() / 2, c.getHeight() / 2);
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public void setHexagonMap(HexagonMap map) {
        this.map = map;
        //this.hexagonMapSizeX = map.getSize()[0];
        //this.hexagonManSizeY = map.getSize()[1];
        this.hexagonMapSizeX = 2000;
        this.hexagonManSizeY = 2000;
    }

    private Bitmap prepareBmp(int resId) {
        // First decode with inJustDecodeBounds=true to check dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDensity = 240;
        options.inTargetDensity = dpiScale;
        options.inScaled = true;
        options.inMutable = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap temp = BitmapFactory.decodeResource(getResources(), resId, options);
        // Then decode in the temp bitmap with lower quality (RGB_565) - since we don't need the alpha channel at all.
        options.inJustDecodeBounds = false;
        options.inBitmap = temp;
        return BitmapFactory.decodeResource(getResources(), resId, options);

    }

    private void prepareTestPaint() {
        wowPaint = new Paint();
        wowPaint.setColor(Color.GREEN);
        wowPaint.setStyle(Paint.Style.FILL);
        borderPaint = new Paint();
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setColor(Color.MAGENTA);
        textPaint = new Paint();
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setColor(Color.BLACK);
        blackPaint = new Paint();
        blackPaint.setColor(Color.BLACK);
        blackPaint.setStyle(Paint.Style.FILL);
    }
}
