package de.blogspot.deckweiss.demiurg.world;


public class Way {
    private String fromNode;
    private String toNode;
    private Knot startingKnot;
    private Knot finishKnot;
    private Knot[] knotArray;
    private int fromX;
    private int fromY;
    private int toX;
    private int toY;

    public Way(String fromN, String toN, Knot[] knotA, int fX, int fY, int tX, int tY){
        fromNode = fromN;
        toNode = toN;
        knotArray = knotA;
        fromX=fX;
        toX=tX;
        fromY=fY;
        toY=tY;
    }

    //<editor-fold desc="getter methods">
    public int getFromY() {
        return fromY;
    }
    public int getFromX() {
        return fromX;
    }
    public int getToX() {
        return toX;
    }
    public int getToY() {
        return toY;
    }
    //</editor-fold>
}
