package de.blogspot.deckweiss.demiurg.world;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

public class MainGameLoop extends Service {
    boolean ready=false;
    BackgroundUpdate backgroundUpdate;
    private final IBinder myBinder = new MyLocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        //Toast.makeText(this, "Service binded", Toast.LENGTH_LONG).show();
        return myBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            //Load data to gameloop
            load();
            ready=true;
        }catch (Exception e){
            ready=false;
            e.printStackTrace();
        }
        if (ready) {
            try {
                backgroundUpdate = new BackgroundUpdate();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        //Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();
        update(); //TODO remove this shit
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        ready=false;
        try {
            //save data to gameloop
            save();
            backgroundUpdate.cancel(true);
        }catch (Exception e){
            e.printStackTrace();
        }
        //Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

    public void load(){
        //TODO load gameloop data
    }

    public void save(){
        //TODO save gameloop data
    }

    public boolean isReady(){return this.ready;}

    public void update(){
        /*
        backgroundUpdate.execute(
                new Integer("1"),
                new Integer("2"),
                new Integer("3"),
                new Integer("4"),
                new Integer("5"));
                */
    }

    private class BackgroundUpdate extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... objectses) {
            int i;
            for (i=0; i<objectses.length; i++) {
                doingShit();
                publishProgress((int)(((i+1)/(float)objectses.length)*100));
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress){
            //Toast.makeText(getBaseContext(),String.valueOf(progress[0])+"%", Toast.LENGTH_LONG).show();
        }

        protected void onPostExecute(Integer result){
            //Toast.makeText(getBaseContext(), "Fücking done", Toast.LENGTH_LONG).show();
        }

        protected void doingShit(){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public class MyLocalBinder extends Binder {
        MainGameLoop getService() {
            return MainGameLoop.this;
        }
    }
}
