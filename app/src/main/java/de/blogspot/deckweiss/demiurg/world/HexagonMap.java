package de.blogspot.deckweiss.demiurg.world;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Stores the hexagon map / structure of a node (village etc.)
 *
 * @author David
 */
public class HexagonMap implements Parcelable {
    private List<Hexagon> hexagons;
    private boolean flat;
    private int[] size = {1000, 1000}; //TODO remove hardcoded values, they are only there for test.

    public HexagonMap(boolean flat) {
        hexagons = new ArrayList<Hexagon>();
//        addHexagon(new Hexagon(0, 0, HexagonContent.WOOD));
//        addHexagon(new Hexagon(0, 1, HexagonContent.WOOD));
//        addHexagon(new Hexagon(0, 2, HexagonContent.WOOD));
//        addHexagon(new Hexagon(0, 3, HexagonContent.WOOD));
//        addHexagon(new Hexagon(0, 4, HexagonContent.WOOD));
//        addHexagon(new Hexagon(0, 5, HexagonContent.WOOD));
//        addHexagon(new Hexagon(1, 1, HexagonContent.WOOD));
    }

    /**
     * {@code flat} defaults to true
     *
     * @see #HexagonMap(boolean)
     */
    public HexagonMap() {
        this(true);
    }

    /**
     * Adds a hexagon to the hexagon map.
     *
     * @param hexagon the hexagon that shall be added
     */
    public void addHexagon(Hexagon hexagon) {
        hexagons.add(hexagon);
    }

    /**
     * Returns a list of the map's hexagons
     *
     * @return a list of the map's hexagons
     */
    public List<Hexagon> getHexagons() {
        return hexagons;
    }

    public boolean isFlat() {
        return flat;
    }

    public void setFlat(boolean flat) {
        this.flat = flat;
    }

    protected HexagonMap(Parcel in) {
        if (in.readByte() == 0x01) {
            hexagons = new ArrayList<Hexagon>();
            in.readList(hexagons, Hexagon.class.getClassLoader());
        } else {
            hexagons = null;
        }
        flat = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (hexagons == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(hexagons);
        }
        dest.writeByte((byte) (flat ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<HexagonMap> CREATOR = new Parcelable.Creator<HexagonMap>() {
        @Override
        public HexagonMap createFromParcel(Parcel in) {
            return new HexagonMap(in);
        }

        @Override
        public HexagonMap[] newArray(int size) {
            return new HexagonMap[size];
        }
    };

    /**
     * The NodeDisplay needs a size in pixel for scrolling the hexagonMap
     *
     * @return Array[xSize, ySize], the exact outer rectangle size of the hexagonMap
     */
    public int[] getDimension() { //I'd still prefer using a "Dimension" class in such situations rather than returning a meaningless array of values
        int minX = 0;
        int maxX = 0;
        int minY = 0;
        int maxY = 0;
        for (int i = 0; i < hexagons.size(); i++) {
            Hexagon hexagon = hexagons.get(i);
            minX = Math.min(minX, hexagon.getX());
            maxX = Math.max(maxX, hexagon.getX());
            minY = Math.min(minY, hexagon.getY());
            maxY = Math.max(maxY, hexagon.getY());
        }

        return new int[]{
                maxX - minX + 1, maxY - minY + 1
        };

        /* This will be returned later when we need more than just the absolute width and height of the map
        return new int[]{
                minX,
                maxX,
                minY,
                maxY
        };
        */
    }
}