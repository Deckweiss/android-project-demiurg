package de.blogspot.deckweiss.demiurg;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.Toast;

import de.blogspot.deckweiss.demiurg.world.MapActivity;

/**
 * @author Deckweiss
 *         Main menu activity
 */
public class MainMenu extends Activity {
    asyncBackgroundMusic bgMusic;
    boolean backgroundMusic;
    boolean backgroundSound;

    //<editor-fold desc="Runnable">

    /**
     * Sets fullscreen, sets a layout, loads the settings from SharedPreferences
     *
     * @param savedInstanceState the last state of the instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fullscreen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main_menu);
        //Load settings from SharedPreferences
        SharedPreferences settings = getSharedPreferences("MyPrefs", 0);
        backgroundMusic = settings.getBoolean("musicOption", true);
        backgroundSound = settings.getBoolean("soundOption", true);
    }

    /**
     * When activity is closed or not on screen it stops the music thread
     */
    @Override
    protected void onPause() {
        super.onPause();

        if (backgroundMusic) {
            bgMusic.stopMusic();
            bgMusic.cancel(true);
        }
    }

    /**
     * When activity is started or resumed it loads the settings and starts the music thread
     */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = getSharedPreferences("MyPrefs", 0);
        backgroundMusic = settings.getBoolean("musicOption", true);
        if (backgroundMusic) {
            bgMusic = new asyncBackgroundMusic();
            bgMusic.execute();
        }
    }
    //</editor-fold>

    //<editor-fold desc="Button clicks">

    /**
     * Goes one menu back or closes the app if the system back button is pressed
     */
    @Override
    public void onBackPressed() {
        String tag = this.findViewById(android.R.id.content).toString();
        if (tag.equals("mainMenu")) {
            finish();
        } else if (tag.equals("savedGamesMenu")) {
            setContentView(R.layout.activity_play_menu);
        } else {
            setContentView(R.layout.activity_main_menu);
        }
    }

    /**
     * Opens the play menu when play button is activated
     *
     * @param view The view that was clicked.
     */
    public void PlayClick(View view) {
        setContentView(R.layout.activity_play_menu);
    }

    /**
     * Starts a new game via Intent when new game button is activated
     *
     * @param view The view that was clicked.
     */
    public void NewGame(View view) {
        //TODO (StartActivityForResult)? List of packs
        //TODO StartActivityForResult CharCreation.main();
        //Starting MainGameLoop service
        //startService(new Intent(getBaseContext(), MainGameLoop.class));

        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    /**
     * Opens the game from the latest save when continue button is activated
     *
     * @param view The view that was clicked.
     */
    public void ContinueGame(View view) {
        //todo continuegame
    }

    /**
     * Opens a menu for loading games when load game button is activated
     *
     * @param view The view that was clicked.
     */
    public void LoadGame(View view) {
        //todo loadgame
    }

    /**
     * Starts an Intent to open the games homepage when button is activated
     *
     * @param view The view that was clicked.
     */
    public void WebClick(View view) {
        String communityUrl = "http://deckweiss.blogspot.de";
        Intent CommunityUrlIntent = new Intent(Intent.ACTION_VIEW);
        CommunityUrlIntent.setData(Uri.parse(communityUrl));
        startActivity(CommunityUrlIntent);
    }

    /**
     * Opens a list of credits when button is activated
     *
     * @param view The view that was clicked.
     */
    public void CreditsClick(View view) {
        setContentView(R.layout.activity_credits);
    }

    /**
     * Exits the app when exit button is activated
     *
     * @param view The view that was clicked.
     */
    public void ExitClick(View view) {
        finish();
    }

    /**
     * Opens the options view when options button is clicked and sets the checkboxes from the saved settings
     *
     * @param view The view that was clicked.
     */
    public void OptionsClick(View view) {
        setContentView(R.layout.activity_options_menu);
        CheckBox checkBoxMusic = (CheckBox) findViewById(R.id.checkBoxMusic);
        CheckBox checkBoxSound = (CheckBox) findViewById(R.id.checkBoxSound);
        SharedPreferences settings = getSharedPreferences("MyPrefs", 0);
        checkBoxMusic.setChecked(settings.getBoolean("musicOption", true));
        checkBoxSound.setChecked(settings.getBoolean("soundOption", true));
    }

    /**
     * Goes to main menu, when the ingame back button is pressed
     *
     * @param view The view that was clicked.
     */
    public void BackClick(View view) {
        setContentView(R.layout.activity_main_menu);
    }
    //</editor-fold>

    //<editor-fold desc="Change music and sound when it is changed in the options (on/off)">

    /**
     * Saves the state of the Music settings to the SharedPreferences when they are changed and updates the music thread
     *
     * @param view The view that was changed.
     */
    public void onOptionsMusicChange(View view) {
        CheckBox checkBoxMusic = (CheckBox) findViewById(R.id.checkBoxMusic);
        SharedPreferences settings = getSharedPreferences("MyPrefs", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("musicOption", checkBoxMusic.isChecked());
        editor.apply();
        backgroundMusic = settings.getBoolean("musicOption", true);
        if (backgroundMusic) {
            bgMusic = new asyncBackgroundMusic();
            bgMusic.execute();
        }
        if (!backgroundMusic) {
            bgMusic.stopMusic();
        }
    }

    /**
     * Saves the state of the Sound settings to the SharedPreferences when they are changed and updates the sound thread
     *
     * @param view The view that was changed.
     */
    public void onOptionsSoundChange(View view) {
        CheckBox checkBoxSound = (CheckBox) findViewById(R.id.checkBoxSound);
        SharedPreferences settings = getSharedPreferences("MyPrefs", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("soundOption", checkBoxSound.isChecked());
        editor.apply();
        backgroundSound = settings.getBoolean("soundOption", true);
        if (backgroundSound) {
            Toast.makeText(getApplicationContext(), "Sound ON", Toast.LENGTH_LONG).show();
        }
        if (!backgroundSound) {
            Toast.makeText(getApplicationContext(), "Sound OFF", Toast.LENGTH_LONG).show();
        }
    }
    //</editor-fold>

    //Class for bgMusic with own Thread
    public class asyncBackgroundMusic extends AsyncTask<Void, Void, Void> {
        MediaPlayer mMediaPlayer;

        protected void onPreExecute() {
            mMediaPlayer = MediaPlayer.create(MainMenu.this, R.raw.calmstreams);
        }

        protected Void doInBackground(Void... params) {
            mMediaPlayer.setLooping(true); // Set looping
            mMediaPlayer.setVolume(100, 100);
            mMediaPlayer.start();
            return null;
        }

        protected void stopMusic() {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            bgMusic.cancel(true);
        }
    }
}
