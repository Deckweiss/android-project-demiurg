package de.blogspot.deckweiss.demiurg.world;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import de.blogspot.deckweiss.demiurg.R;


public class NodeActivity extends Activity implements View.OnTouchListener {

    private Node node;
    private NodeDisplay nodeSurfaceView;
    private float xLast, yLast, xStart, yStart;
    private boolean isMoving, isClick;
    private static float xShift, yShift;
    private int bgWidth, bgHeight;



    //<editor-fold desc="Runnable">

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fullscreen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        Intent intent = getIntent();
        node = intent.getParcelableExtra("a_node");
        setContentView(R.layout.node_view);
        Toast.makeText(this, node.getName(), Toast.LENGTH_LONG).show();

        nodeSurfaceView = (NodeDisplay) findViewById(R.id.nodeSurfaceView);
        nodeSurfaceView.setOnTouchListener(this);
        nodeSurfaceView.setNode(node);


        nodeSurfaceView.setHexagonMap(node.getMap());
        //bgHeight = (int) node.getMap().getSize()[0];
        //bgWidth = (int) node.getMap().getSize()[1];
        bgHeight = 2000;
        bgWidth = 2000;
    }

    @Override
    protected void onPause() {
        super.onPause();
        nodeSurfaceView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        nodeSurfaceView.resume();
    }


    //</editor-fold>

    public static float getxShift() {
        return xShift;
    }

    public static float getyShift() {
        return yShift;
    }
    //<editor-fold desc="Touch">

    public void touchAt(float xT, float yT) {
        /*
        Node touchedNode = mapDisplayView.touchAt(xT, yT);
        if (touchedNode != null) {
            Intent intent = new Intent(this, NodeActivity.class);
            intent.putExtra("a_node", touchedNode);
            startActivity(intent);
        }*/
    } //TODO TO FUCKING DOOOOOOoooo

    @Override
    public boolean onTouch(View v, MotionEvent me) {
        try {
            Thread.sleep(32);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        switch (me.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xStart = me.getX();
                yStart = me.getY();
                isMoving = true;
                isClick = true;

                break;
            case MotionEvent.ACTION_MOVE:
                if (isMoving) {
                    if (me.getX() - xStart < -20 || me.getX() - xStart > 20 || me.getY() - yStart < -20 || me.getY() - yStart > 20) {
                        isClick = false;
                    }
                    xShift = me.getX() - xStart + xLast;
                    yShift = me.getY() - yStart + yLast;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (isClick) {
                    this.touchAt(me.getX(), me.getY());
                }
                isMoving = false;
                xLast = me.getX() - xStart + xLast;
                yLast = me.getY() - yStart + yLast;
                break;
            default:
                break;
        }


        if (xShift > 0) {
            xShift = 0;
            xLast = 0;
            xStart = me.getX();
        }
        if (yShift > 0) {
            yShift = 0;
            yLast = 0;
            yStart = me.getY();
        }

        if (xShift < -bgWidth + v.getWidth()) {
            xShift = -bgWidth + v.getWidth();
            xLast = -bgWidth + v.getWidth();
            xStart = me.getX();
        }
        if (yShift < -bgHeight + v.getHeight()) {
            yShift = -bgHeight + v.getHeight();
            yLast = -bgHeight + v.getHeight();
            yStart = me.getY();
        }

        return true;
    }
    //</editor-fold>
}
