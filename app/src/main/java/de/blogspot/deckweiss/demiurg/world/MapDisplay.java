package de.blogspot.deckweiss.demiurg.world;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import de.blogspot.deckweiss.demiurg.R;

public class MapDisplay extends SurfaceView implements Runnable {
    private Bitmap bmpBackground, bmpNode;
    private Thread t = null;
    private SurfaceHolder holder;
    private boolean isItOK;
    private Canvas c;
    private float xShift, yShift;
    private float xDpiScale, yDpiScale;
    private int dpiScale;
    private float mScaleFactor = 1.f;
    private float focusX;
    private float focusY;
    private Node[] nodeArray;
    private Way[] wayArray;
    private Node touchedNode;
    private Paint nodeNamePaint;
    private Paint wayPaint;

    public MapDisplay(Context context, Node[] node, Way[] ways) {
        super(context);
        xShift = yShift = focusX = focusY = 0;
        holder = getHolder();
        nodeArray = node;
        wayArray = ways;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        xDpiScale = metrics.xdpi / 240;
        yDpiScale = metrics.ydpi / 240;
        dpiScale = metrics.densityDpi;
        //Log.d("dpi", "dpi:"+metrics.densityDpi+" x:"+metrics.xdpi+" y:"+metrics.ydpi); //Test shows: dpi:240 x:240.0 y:240.0

        bmpNode = prepareBmp(R.drawable.node);
        bmpBackground = prepareBmp(R.drawable.map);
        this.prepareNodeNamePaint();
        this.prepareWayPaint();
    }

    //<editor-fold desc="Runnable">
    @Override
    public void run() {
        //invalidate
        while (isItOK) {
            if (!holder.getSurface().isValid()) {
                continue;
            }

            try {
                Thread.sleep(32);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            xShift = MapActivity.getxShift();
            yShift = MapActivity.getyShift();


            c = holder.lockCanvas();
            c.scale(xDpiScale,yDpiScale,0,0);
            //c.scale(mScaleFactor, mScaleFactor, focusX, focusY);
            c.translate(xShift,yShift);
            //<editor-fold desc="drawing all the things!">

            c.drawBitmap(bmpBackground, 0, 0, null);

            for (int i = 0; i < wayArray.length; i++) {
                this.drawWay(i);
            }

            for (int i = 0; i < nodeArray.length; i++) {
                float centeredNodeX = (nodeArray[i].getxPos() - (bmpNode.getWidth() / 2));
                float centeredNodeY = (nodeArray[i].getyPos() - (bmpNode.getHeight() / 2));
                float centeredTextX = (centeredNodeX - (nodeNamePaint.measureText(nodeArray[i].getName()) - bmpNode.getWidth()) / 2);

                c.drawBitmap(bmpNode, centeredNodeX, centeredNodeY, null);     //Draws node icon, TODO different icons for city, open node etc.
                c.drawText(nodeArray[i].getName(), centeredTextX, centeredNodeY - 7, nodeNamePaint);
            }

            //</editor-fold>
            holder.unlockCanvasAndPost(c);
        }
    }

    public void pause() {
        isItOK = false;
        while (true) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;
        }
        t = null;
    }

    public void resume() {
        isItOK = true;
        t = new Thread(this);
        t.start();
    }
    //</editor-fold>

    private void drawWay(int i) {
        c.drawLine(wayArray[i].getFromX(), wayArray[i].getFromY(), wayArray[i].getToX(), wayArray[i].getToY(), wayPaint);
    }

    private Bitmap prepareBmp(int resId) {
        // First decode with inJustDecodeBounds=true to check dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDensity = 240;
        options.inTargetDensity = dpiScale;
        options.inScaled = true;
        options.inMutable = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap temp = BitmapFactory.decodeResource(getResources(), resId, options);
        // Then decode in the temp bitmap with lower quality (RGB_565) - since we don't need the alpha channel at all.
        options.inJustDecodeBounds = false;
        options.inBitmap = temp;
        return BitmapFactory.decodeResource(getResources(), resId, options);

    }


    private void prepareNodeNamePaint() {
        Typeface tf = Typeface.create("Helvetica", Typeface.NORMAL);
        nodeNamePaint = new Paint();
        nodeNamePaint.setColor(Color.WHITE);
        nodeNamePaint.setStyle(Paint.Style.FILL);
        nodeNamePaint.setTextSize(25);
        nodeNamePaint.setTypeface(tf);
    }

    private void prepareWayPaint() {
        wayPaint = new Paint();
        wayPaint.setColor(Color.GREEN);
        wayPaint.setStyle(Paint.Style.STROKE);
        wayPaint.setStrokeCap(Paint.Cap.ROUND);
        wayPaint.setStrokeWidth(6 * (dpiScale / 240));
    }

    public Node touchAt(float xT, float yT) {
        search:
        for (int i = 0; i < nodeArray.length; i++) {
            if (xT != 0 && yT != 0) {
                if (xT >= ((nodeArray[i].getxPos() - (bmpNode.getWidth() / 2)) * xDpiScale +xShift) &&
                        xT <= ((nodeArray[i].getxPos() - (bmpNode.getWidth() / 2)) *xDpiScale +xShift) + bmpNode.getWidth() &&
                        yT >= ((nodeArray[i].getyPos() - (bmpNode.getHeight() / 2)) *yDpiScale +yShift) &&
                        yT <= ((nodeArray[i].getyPos() - (bmpNode.getHeight() / 2)) *yDpiScale +yShift) + bmpNode.getHeight()) {
                    touchedNode = nodeArray[i];
                    break search;
                } else {
                    touchedNode = null;
                }
            }
        }
        return touchedNode;
    }

    //<editor-fold desc="Getters & Setters">
    public int getBgWidth() {
        return bmpBackground.getWidth();
    }

    public int getBgHeight() {
        return bmpBackground.getHeight();
    }

    public void setFocusY(float focusY) {
        this.focusY = focusY;
    }

    public void setFocusX(float focusX) {
        this.focusX = focusX;
    }

    public void setmScaleFactor(float mScaleFactor) {
        this.mScaleFactor = mScaleFactor;
    }

    public void setyShift(float yShift) {
        this.yShift = yShift;
    }

    public void setxShift(float xShift) {
        this.xShift = xShift;
    }
    //</editor-fold>
}

/*
Notiz:

private HashMap <String, Bitmap> bmpHashMap = new HashMap <String, Bitmap>();
for (int i=0; i<nodeArray.length; i++){
    bmpHashMap.put(nodeArray[i].getId(),BitmapFactory.decodeResource(getResources(),
    getResources().getIdentifier(String.valueOf(i), "drawable", "de.blogspot.deckweiss.demiurg")));
    }
*/