package de.blogspot.deckweiss.demiurg.world;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLHandling extends DefaultHandler {

    public XMLdataManager mDataManager=new XMLdataManager();
    private StringBuilder mStringBuilder = new StringBuilder();
    private String tempNodeId = "";
    private int tempXPos = 0;
    private int tempYPos = 0;
    private int tempHexX = 0;
    private int tempHexY = 0;
    private String tempNodeName = "";
    private String tempNodeDescription = "";
    private String tempWayF = "";
    private String tempWayT = "";
    private String tempLinkTo = "";
    private String tempLinkS = "";
    private String tempKnotId = "";
    private String tempKnotEvent = "";
    private HexagonMap tempHexagonMap = null;
    private boolean inHexagon;
    private String tempHexContent = "";
    private Hexagon tempHexagon = null;

    // TODO tempNodeContent
    //private boolean inNodes, inNode, inX, inY, inName, inDescription, inContent, inHexmap = false;
    //private boolean inWays, inWay, inKnot, inEvents, inLinks, inLink = false;

    public XMLdataManager getDataManager(){return mDataManager;}

    @Override
    public void startDocument() throws SAXException {}

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        mStringBuilder.setLength(0);
        if ("Nodes".equalsIgnoreCase(localName)) {
            /*inNodes = true;*/
        }
        else if ("Node".equalsIgnoreCase(localName)) {
            /*inNode = true;*/
            tempNodeId = attributes.getValue("id");
        } else if ("x".equalsIgnoreCase(localName) && !inHexagon) {/*inX = true;*/} else if ("y".equalsIgnoreCase(localName) && !inHexagon) {/*inY = true;*/} else if ("name".equalsIgnoreCase(localName)) {/*inName = true;*/} else if ("nodemenu".equalsIgnoreCase(localName)) {/*inDescription = true;*/} else if ("content".equalsIgnoreCase(localName) && !inHexagon) {
            /**/
        }
        else if ("way".equalsIgnoreCase(localName)){
            tempWayF=attributes.getValue("NodeF");
            tempWayT=attributes.getValue("NodeT");
        }
        else if ("link".equalsIgnoreCase(localName)){
            tempLinkTo = attributes.getValue("to");
        }
        else if("knot".equalsIgnoreCase(localName)){
            tempKnotId =attributes.getValue("id");
        } else if ("hexagonMap".equalsIgnoreCase(localName)) {
            /*inHexagonMap=true;*/
            tempHexagonMap = new HexagonMap();
        } else if ("hexagon".equalsIgnoreCase(localName)) {
            inHexagon = true;
        } else if ("x".equalsIgnoreCase(localName) && inHexagon) {
            /**/
        } else if ("y".equalsIgnoreCase(localName) && inHexagon) {
            /**/
        } else if ("content".equalsIgnoreCase(localName) && inHexagon) {
            /**/
        } else if ("flat".equalsIgnoreCase(localName)) {
            /**/
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        mStringBuilder.append(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("Nodes".equalsIgnoreCase(localName)) {/*inNodes = false;*/}
        else if ("Node".equalsIgnoreCase(localName)) {
            mDataManager.addNode(tempNodeId, tempXPos, tempYPos, tempNodeName, tempNodeDescription, tempHexagonMap);
            /*inNode = false;*/
        } else if ("x".equalsIgnoreCase(localName) && !inHexagon) {
            tempXPos =Integer.parseInt(mStringBuilder.toString().trim());
            /*inX = false;*/
        } else if ("y".equalsIgnoreCase(localName) && !inHexagon) {
            tempYPos =Integer.parseInt(mStringBuilder.toString().trim());
            /*inY = false;*/
        } else if ("content".equalsIgnoreCase(localName) && !inHexagon) {/*inContent = false;*/}
        else if ("name".equalsIgnoreCase(localName)) {
            tempNodeName = mStringBuilder.toString().trim();
            /*inName = false;*/
        } else if ("nodemenu".equalsIgnoreCase(localName)) {
            tempNodeDescription = mStringBuilder.toString().trim();
            /*inDescription = false;*/
        }
        //else if ("hexmap".equalsIgnoreCase(localName)) {/*inHexmap = false;*/}
        else if("way".equalsIgnoreCase(localName)){
            mDataManager.addWay(tempWayF, tempWayT);
        }
        else if ("link".equalsIgnoreCase(localName)){
            tempLinkS = mStringBuilder.toString().trim();
            mDataManager.addLink(tempLinkTo,tempLinkS);
        }
        else if ("knot".equalsIgnoreCase(localName)){
            mDataManager.addKnot(tempKnotId,tempKnotEvent);
        }
        else if("event".equalsIgnoreCase(localName)){
            tempKnotEvent = mStringBuilder.toString().trim();
        } else if ("hexagonMap".equalsIgnoreCase(localName)) {
            /*inHexagonMap=false;*/
        } else if ("hexagon".equalsIgnoreCase(localName)) {
            tempHexagon = new Hexagon(tempHexX, tempHexY, HexagonContent.valueOf(tempHexContent));
            tempHexagonMap.addHexagon(tempHexagon);
            inHexagon = false;
        } else if ("x".equalsIgnoreCase(localName) && inHexagon) {
            tempHexX = Integer.parseInt(mStringBuilder.toString().trim());
            /*inX = false;*/
        } else if ("y".equalsIgnoreCase(localName) && inHexagon) {
            tempHexY = Integer.parseInt(mStringBuilder.toString().trim());
            /*inY = false;*/
        } else if ("content".equalsIgnoreCase(localName) && inHexagon) {
            tempHexContent = mStringBuilder.toString().trim();
        /*inContent = false;*/
        } else if ("flat".equalsIgnoreCase(localName)) {
            tempHexagonMap.setFlat(Boolean.valueOf(mStringBuilder.toString().trim()));
        }
    }

    @Override
    public void endDocument() throws SAXException {
    }
}