package de.blogspot.deckweiss.demiurg.charakter;

public abstract class Character {

    private int might;
    private int dex;
    private int mastery;

    private int leadership;
    private int diplomacy;
    private int empathy;

    private int pathfinding;
    private int observation;
    private int knowledge;

    private String name;
    private Race race;

    //<editor-fold desc="Konstruktoren der Klasse">
    /**
     * Konstruktoren.
     */
    public Character(){
        might = dex = mastery = leadership = diplomacy
                = empathy = pathfinding = observation = knowledge =0;
        this.name = "testdummie";
        this.race = new Race();
    }

    public Character(String name){
        might = dex = mastery = leadership = diplomacy
        = empathy = pathfinding = observation = knowledge =0;

        this.name = name;
        this.race = new Race();
    }

    public Character(int might, int dex, int mastery, int leadership,
                        int diplomacy, int empathy, int pathfinding, int observation,
                        int knowledge, String name, Race race) {
        this.might = might;
        this.dex = dex;
        this.mastery = mastery;
        this.leadership = leadership;
        this.diplomacy = diplomacy;
        this.empathy = empathy;
        this.pathfinding = pathfinding;
        this.observation = observation;
        this.knowledge = knowledge;
        this.name = name;
        this.race = race;
    }
    //</editor-fold>

    //<editor-fold desc="Alle Getter methoden.">
    /**
     * Getter Methoden.
     * @return  wert Des Stats.
     */

    public int getMight() {
        return might;
    }

    public int getDex() {
        return dex;
    }

    public int getMastery() {
        return mastery;
    }

    public int getLeadership() {
        return leadership;
    }

    public int getDiplomacy() {
        return diplomacy;
    }

    public int getEmpathy() {
        return empathy;
    }

    public int getPathfinding() {
        return pathfinding;
    }

    public int getObservation() {
        return observation;
    }

    public int getKnowledge() {
        return knowledge;
    }

    public String getName() {
        return name;
    }

    public Race getRace() {
        return race;
    }
    //</editor-fold>

    //<editor-fold desc="Alle Setter methoden">
    public void setMight(int might) {
        this.might = might;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public void setMastery(int mastery) {
        this.mastery = mastery;
    }

    public void setLeadership(int leadership) {
        this.leadership = leadership;
    }

    public void setDiplomacy(int diplomacy) {
        this.diplomacy = diplomacy;
    }

    public void setEmpathy(int empathy) {
        this.empathy = empathy;
    }

    public void setPathfinding(int pathfinding) {
        this.pathfinding = pathfinding;
    }

    public void setObservation(int observation) {
        this.observation = observation;
    }

    public void setKnowledge(int knowledge) {
        this.knowledge = knowledge;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRace(Race race) {
        this.race = race;
    }
    //</editor-fold>

    //<editor-fold desc="Alle Raise methoden für stat + 1">
    private void raiseMight(){
        this.might++;
    }
    private void raiseDex(){
        this.dex++;
    }
    private void raiseMastery(){
        this.mastery++;
    }
    private void raiseLeadership(){
        this.leadership++;
    }
    private void raiseDiplomacy(){
        this.diplomacy++;
    }
    private void raiseEmpathy(){
        this.empathy++;
    }
    private void raisePathfinding(){
        this.pathfinding++;
    }
    private void raiseObservation(){
        this.observation++;
    }
    private void raiseKnowledge(){
        this.knowledge++;
    }
    //</editor-fold>

    //<editor-fold desc="Alle RaiseBy methoden für stat + wert">
    private void raiseMightBy(int wert){
        this.might = this.might + wert;
    }
    private void raiseDexBy(int wert){
        this.dex = this.dex + wert;
    }
    private void raiseMasteryBy(int wert){
        this.mastery = this.mastery + wert;
    }
    private void raiseLeadershipBy(int wert){
        this.leadership = this.leadership + wert;
    }
    private void raiseDiplomacyBy(int wert){
        this.diplomacy = this.diplomacy + wert;
    }
    private void raiseEmpathyBy(int wert){
        this.empathy = this.empathy + wert;
    }
    private void raisePathfindingBy(int wert){
        this.pathfinding = this.pathfinding + wert;
    }
    private void raiseObservationBy(int wert){
        this.observation = this.observation + wert;
    }
    private void raiseKnowledgeBy(int wert){
        this.knowledge = this.knowledge + wert;
    }
    //</editor-fold>


}
