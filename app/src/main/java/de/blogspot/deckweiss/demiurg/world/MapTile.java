package de.blogspot.deckweiss.demiurg.world;

import android.graphics.Bitmap;

/**
 * This class is the base class of all classes which can be drawn on the hexagon map of a node.
 *
 * @author David
 */
public abstract class MapTile {
    private String description;
    private boolean walkable;

    public String getDescription() {
        return description;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public abstract String getString();

    public abstract Bitmap getImage();

    public abstract void onClick();
}
