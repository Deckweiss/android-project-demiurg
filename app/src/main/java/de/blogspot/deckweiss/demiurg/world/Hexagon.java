package de.blogspot.deckweiss.demiurg.world;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a hexagon as a tile of a hexagon map.
 *
 * @author David
 */
public class Hexagon implements Parcelable {

    private int x;
    private int y;
    private HexagonContent content;


    /**
     * Creates a hexagon at the specified coordinates and content.
     *
     * @param x       the x-position of the hexagon.
     * @param y       the y-position of the hexagon.
     * @param content the content of the hexagon
     */
    public Hexagon(int x, int y, HexagonContent content) {
        this.x = x;
        this.y = y;
        this.content = content;
    }

    public Hexagon() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public HexagonContent getContent() {
        return content;
    }

    public void setContent(HexagonContent content) {
        this.content = content;
    }

    protected Hexagon(Parcel in) {
        x = in.readInt();
        y = in.readInt();
        content = (HexagonContent) in.readValue(HexagonContent.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(x);
        dest.writeInt(y);
        dest.writeValue(content);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Hexagon> CREATOR = new Parcelable.Creator<Hexagon>() {
        @Override
        public Hexagon createFromParcel(Parcel in) {
            return new Hexagon(in);
        }

        @Override
        public Hexagon[] newArray(int size) {
            return new Hexagon[size];
        }
    };
}