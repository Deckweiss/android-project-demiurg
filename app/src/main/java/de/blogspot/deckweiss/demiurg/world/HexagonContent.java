package de.blogspot.deckweiss.demiurg.world;

/**
 * Temporary enum for storing hexagon types like EMPTY or WOOD.
 *
 * @author David
 */
public enum HexagonContent {
    EMPTY, WOOD, WOODCUTTERS_HUT
}
