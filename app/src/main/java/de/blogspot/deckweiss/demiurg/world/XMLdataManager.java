package de.blogspot.deckweiss.demiurg.world;

import java.util.ArrayList;
import java.util.Arrays;

public class XMLdataManager {
    private Link[] linkArray;
    private Knot[] knotArray;
    private Way[] wayArray;
    private Node[] nodeArray;

    public XMLdataManager(){
        linkArray = new Link[]{new Link("", "")};
        knotArray = new Knot[]{new Knot("", "", linkArray)};
        wayArray = new Way[]{new Way("", "", knotArray,-100,-100,-100,-100)};
        nodeArray = new Node[]{new Node("nNULL", -100, -100, "NULL", "NULL", null)};
    }

    //<editor-fold desc="appendToArray methods">
    private static Node[] appendToNodeArray(Node[] array, Node node) {
        ArrayList<Node> temp = new ArrayList<Node>(Arrays.asList(array));
        temp.add(node);
        return temp.toArray(new Node[temp.size()]);
    }

    private static Way[] appendToWayArray(Way[] array, Way way) {
        ArrayList<Way> temp = new ArrayList<Way>(Arrays.asList(array));
        temp.add(way);
        return temp.toArray(new Way[temp.size()]);
    }

    private static Knot[] appendToKnotArray(Knot[] array, Knot knot) {
        ArrayList<Knot> temp = new ArrayList<Knot>(Arrays.asList(array));
        temp.add(knot);
        return temp.toArray(new Knot[temp.size()]);
    }

    private static Link[] appendToLinkArray(Link[] array, Link link) {
        ArrayList<Link> temp = new ArrayList<Link>(Arrays.asList(array));
        temp.add(link);
        return temp.toArray(new Link[temp.size()]);
    }
    //</editor-fold>

    //<editor-fold desc="add methods">
    public void addNode(String id, int x, int y, String name, String description, HexagonMap tempHexagonMap) {
        nodeArray = appendToNodeArray(nodeArray, new Node(id, x, y, name, description, tempHexagonMap));
    }
    public void addWay(String fromN, String toN){
        int fX,fY,tX,tY;
        Node from = this.findNodeById(fromN);
        Node to = this.findNodeById(toN);
        if(from == null||to==null){
            fX=-100;
            fY=-100;
            tX=-100;
            tY=-100;
        }
        else{
            fX=(int)from.getxPos();
            fY=(int)from.getyPos();
            tX=(int)to.getxPos();
            tY=(int)to.getyPos();
        }
        wayArray = appendToWayArray(wayArray, new Way(fromN,toN,knotArray, fX,fY,tX,tY));
    }

    public void addKnot(String kID, String ev){
        knotArray = appendToKnotArray(knotArray, new Knot(kID,ev,linkArray));
    }
    public void addLink(String to, String sId){
        linkArray = appendToLinkArray(linkArray, new Link(to,sId));
    }
    //</editor-fold>

    //<editor-fold desc="getter methods">
    public Node[] getNodeArray() {
        return nodeArray;
    }
    public Way[] getWayArray() {
        return wayArray;
    }
    public Knot[] getKnotArray() {
        return knotArray;
    }
    public Link[] getLinkArray() {
        return linkArray;
    }
    //</editor-fold>

    //If this stops working add label back to outer loop
    private Node findNodeById(String id) {
        Node mNode = null;
        for (Node aNodeArray : nodeArray) {
            if (aNodeArray.getId().equals(id)) {
                mNode = aNodeArray;
                break;
            } else {
                mNode = null;
            }
        }
        return mNode;
    }
}

