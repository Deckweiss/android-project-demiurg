package de.blogspot.deckweiss.demiurg.charakter;

import de.blogspot.deckweiss.demiurg.world.Node;

public class PlayerCharacter extends Character {

    private int hP;
    private int mP;
    private Node location;
//    private int exP;

    //<editor-fold desc="Alle Konstruktoren">
    public PlayerCharacter (){
        super();
        this.hP = 100;
        this.mP = 100;
//        this.exP = 0;
    }

    public PlayerCharacter (String name){
        super(name);
        this.hP = 100;
        this.mP = 100;
//        this.exP = 0;
    }

    public PlayerCharacter (int might, int dex, int mastery, int leadership,
                            int diplomacy, int empathy, int pathfinding, int observation,
                            int knowledge, String name, Race race){

        super(might, dex, mastery, leadership, diplomacy, empathy,
                pathfinding, observation, knowledge, name ,race);
        this.hP = 100;
        this.mP = 100;
//        this.exP = 0;
    }

    //</editor-fold>


    //<editor-fold desc="Alle Getter methoden">
    public int gethP() {
        return hP;
    }

    public int getmP() {
        return mP;
    }

//    public int getExP() {
//        return exP;
//    }

    //</editor-fold>

    public boolean reduceHP(int wert){
        this.hP = this.hP - wert;
        return (hP > 0);
    }

//    public void raiseExPBy(int wert){
//        this.exP = this.exP + wert;
//    }
}
