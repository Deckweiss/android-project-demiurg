package de.blogspot.deckweiss.demiurg.world;


import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Node implements Parcelable {
    private float xPos;
    private float yPos;
    private String id;
    private String name;
    private String description;
    private HexagonMap map;
    // TODO nodeContent
    // TODO Hexmap

    public Node(String id, int xPos, int yPos, String name, String description) {
        this(id, xPos, yPos, name, description, new HexagonMap());
    }

    public Node(String id, int xPos, int yPos, String name, String description, HexagonMap map) {
        this.id = id;
        this.xPos = xPos;
        this.yPos = yPos;
        this.name = name;
        this.description = description;
        this.map = map;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getyPos() {
        return yPos;
    }

    public float getxPos() {
        return xPos;
    }

    public HexagonMap getMap() {
        return map;
    }

    public String getId() {
        return id;
    }

    protected Node(Parcel in) {
        xPos = in.readFloat();
        yPos = in.readFloat();
        id = in.readString();
        name = in.readString();
        description = in.readString();
        map = (HexagonMap) in.readValue(HexagonMap.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(xPos);
        dest.writeFloat(yPos);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeValue(map);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Node> CREATOR = new Parcelable.Creator<Node>() {
        @Override
        public Node createFromParcel(Parcel in) {
            return new Node(in);
        }

        @Override
        public Node[] newArray(int size) {
            return new Node[size];
        }
    };
}
