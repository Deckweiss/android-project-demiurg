package de.blogspot.deckweiss.demiurg.world;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MapActivity extends Activity implements View.OnTouchListener {

    private static final int INVALID_POINTER_ID = -1;
    private float mLastTouchX;
    private float mLastTouchY;
    private int mActivePointerId = INVALID_POINTER_ID;
    private float mScaleFactor = 1.f;
    private float focusX;
    private float focusY;
    private float lastFocusX = -1;
    private float lastFocusY = -1;
    private static float xShift, yShift;            // old
    private float xLast, yLast, xStart, yStart;     // old
    private boolean isMoving, isClick;              // old
    private int bgWidth, bgHeight;                  // old
    Matrix matrix;
    float sy;
    float sx;

    private ScaleGestureDetector mScaleDetector;

    MapDisplay mapDisplayView;
    private XMLdataManager mDataManager;
    private MainGameLoop myService;
    private boolean isBound = false;

    //<editor-fold desc="Runnable">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fullscreen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        this.startXMLparser();
        mapDisplayView = new MapDisplay(MapActivity.this.getApplicationContext(), mDataManager.getNodeArray(), mDataManager.getWayArray());
        mapDisplayView.setOnTouchListener(this);
        xShift = yShift = xLast = yLast = xStart = yStart = 0;
        isMoving = isClick = false;
        bgHeight = mapDisplayView.getBgHeight();
        bgWidth = mapDisplayView.getBgWidth();

        this.setContentView(mapDisplayView);
        //Toast.makeText(getBaseContext(),"Mapview created", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(getBaseContext(),"Mapview paused", Toast.LENGTH_LONG).show();

        stopService(new Intent(getBaseContext(), MainGameLoop.class));
        this.unbindService(myConnection);

        mapDisplayView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(getBaseContext(),"Mapview resumed", Toast.LENGTH_LONG).show();

        bindService(new Intent(this, MainGameLoop.class), myConnection, Context.BIND_AUTO_CREATE);
        startService(new Intent(this, MainGameLoop.class));

        mapDisplayView.resume();
    }
    //</editor-fold>

    //<editor-fold desc="Methods">
    private void startXMLparser() {
        try {
            //getting XML reader to parse data
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();
            XMLHandling doWork = new XMLHandling();
            xr.setContentHandler(doWork);
            InputSource source = new InputSource(getAssets().open("world.xml"));
            xr.parse(source);
            mDataManager = doWork.getDataManager();
            //(String.valueOf(istr))

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static float getxShift() {
        return xShift;
    }

    public static float getyShift() {
        return yShift;
    }
    //</editor-fold>

    //<editor-fold desc="Touch">
    public void touchAt(float xT, float yT) {
        Node touchedNode = mapDisplayView.touchAt(xT, yT);
        if (touchedNode != null) {
            Intent intent = new Intent(this, NodeActivity.class);
            intent.putExtra("a_node", touchedNode);
            startActivity(intent);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent me) {
        try {
            Thread.sleep(32);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        switch (me.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xStart = me.getX();
                yStart = me.getY();
                isMoving = true;
                isClick = true;

                break;
            case MotionEvent.ACTION_MOVE:
                if (isMoving) {
                    if (me.getX() - xStart < -20 || me.getX() - xStart > 20 || me.getY() - yStart < -20 || me.getY() - yStart > 20) {
                        isClick = false;
                    }
                    xShift = me.getX() - xStart + xLast;
                    yShift = me.getY() - yStart + yLast;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (isClick) {
                    this.touchAt(me.getX(), me.getY());
                }
                isMoving = false;
                xLast = me.getX() - xStart + xLast;
                yLast = me.getY() - yStart + yLast;
                break;
            default:
                break;
        }


        if (xShift > 0) {
            xShift = 0;
            xLast = 0;
            xStart = me.getX();
        }
        if (yShift > 0) {
            yShift = 0;
            yLast = 0;
            yStart = me.getY();
        }

        if (xShift < -bgWidth + v.getWidth()) {
            xShift = -bgWidth + v.getWidth();
            xLast = -bgWidth + v.getWidth();
            xStart = me.getX();
        }
        if (yShift < -bgHeight + v.getHeight()) {
            yShift = -bgHeight + v.getHeight();
            yLast = -bgHeight + v.getHeight();
            yStart = me.getY();
        }

        //Toast.makeText(this,, Toast.LENGTH_LONG).show();

        return true;
    }
    //</editor-fold>

    private ServiceConnection myConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            MainGameLoop.MyLocalBinder binder = (MainGameLoop.MyLocalBinder) service;
            myService = binder.getService();
            isBound = true;
        }

        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
        }

    };
}
